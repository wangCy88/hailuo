<?php
define('PIC_URL', \think\Request::instance()->domain());
define('IS_POST', \think\Request::instance()->isPost());
define('__API__', '/public/static/api/');
return [
    'default_return_type' => 'json',
    'url_route_on' => true,
    'url_route_must' => false,
    'default_head' => '/public/static/api/default_head/default_head.png',
    //分享参数
    'share' => [
        'halo_logo' => PIC_URL . '/public/static/api/images/logoHL.png',
        'title' => '您的朋友已成功入驻“海螺”！他正呼唤你！“得脉者，得机遇”,在海螺有您想要的商业价值。',
        'content' => '海螺是一款面向全国高端商务人士的社交平台，专注于商务意见领袖、财经大咖及高端行业精英聚合和商务社交的平台，利用互联网思维实现资源共享，知识共享，人才共享，打造一个良性互动的商界高端社区。',
        'url' => 'https://fir.im/dkvn',
    ],

];
